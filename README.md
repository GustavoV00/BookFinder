# BookFinder

Web app made using the google book api

## Technologies

React (States and Context API) and axios to consume de Google Book Api

## Note

I have used some random colors, the objective was just do something with react.

## How to use:

`npm install` and `npm start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in your browser.

The page will reload when you make changes.\
